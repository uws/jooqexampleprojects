package jooq.h2

import ie.uws.Book
import ie.uws.example.public_.Tables
import ie.uws.example.public_.tables.records.BookRecord
import org.jooq.DSLContext

class BookOperations {

    /*
     * General notice: passing service just for better showing there is no 'magic' with injecting.
     *
     */

    static void insertBooks(def jooqService) {
        Book.withTransaction {
            BookRecord record = jooqService.dataSource.newRecord(Tables.BOOK)
            record.author = "John"
            record.name = "Hobbit"
            record.version = 1
            record.store()
        }
    }

    /**
     * Connection is released by Grails when transaction is completed or action
     * is completed. In example below, new connection is created for each loop
     * iteration. In short period application will hit connections limit and
     * hang. You should not use it that way, instead please see how to resolve
     * this via explicit `transaction` or how to `reuse` dataSource.
     */
    static void willHang(def jooqService) {
        (1..10000).each { index ->
            BookRecord record = jooqService.dataSource.newRecord(Tables.BOOK)
            record.author = "John-${index}"
            record.name = "Hobbindex-${index}"
            record.version = 1
            record.store()
            println index
        }
    }

    static void wontHang(BookService bookService) {
        bookService.wontHung()
    }

    static void ok(def jooqService) {
        Book.withTransaction {
            BookRecord record = jooqService.dataSource.newRecord(Tables.BOOK)
            record.author = "TR_1"
            record.name = "name"
            record.version = 1
            record.store()
            record = jooqService.dataSource.newRecord(Tables.BOOK)
            record.author = "TR_2"
            record.name = "name"
            record.version = 1
            record.store()
        }
    }

    static void rollback(def jooqService) {
        Book.withTransaction {
            BookRecord record = jooqService.dataSource.newRecord(Tables.BOOK)
            record.author = "roolback_1"
            record.name = "name"
            record.version = 1
            record.store()
            throw new RuntimeException() // rollback changes
            record = jooqService.dataSource.newRecord(Tables.BOOK)
            record.author = "roolback_1"
            record.name = "name"
            record.version = 1
            record.store()
        }
    }

    static void transaction(def jooqService) {
        (1..10000).each { index ->
            Book.withTransaction {
                BookRecord record = jooqService.dataSource.newRecord(Tables.BOOK)
                record.author = "John-${index}"
                record.name = "Hobbindex-${index}"
                record.version = 1
                record.store()
                println index
            }
        }
    }

    static void reuse(def jooqService) {
        DSLContext dataSource = jooqService.dataSource
        (1..10000).each { index ->
            BookRecord record = dataSource.newRecord(Tables.BOOK)
            record.author = "John-${index}"
            record.name = "Hobbindex-${index}"
            record.version = 1
            record.store()
            println index
        }
    }

}
