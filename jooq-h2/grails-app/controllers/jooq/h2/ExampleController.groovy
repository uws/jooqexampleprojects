package jooq.h2
import ie.uws.Book

class ExampleController {
    def jooqService

    def index() {
        render view: 'index', model: [
            books: Book.list()
        ]
    }

    def insertBook() {
        BookOperations.insertBooks(jooqService)
        redirect(controller: 'example')
    }

}
