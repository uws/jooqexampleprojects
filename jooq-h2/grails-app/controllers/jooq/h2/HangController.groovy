package jooq.h2

class HangController {
    def jooqService
    def bookService

    static defaultAction = "willHang"

    /**
     * Connection is released by Grails when transaction is completed or action
     * is completed. In example below, new connection is created for each loop
     * iteration. In short period application will hit connections limit and
     * hang. You should not use it that way, instead please see how to resolve
     * this via explicit `transaction` or how to `reuse` dataSource.
     */
    def willHang() {
        BookOperations.willHang(jooqService)
        redirect(controller: 'example')
    }

    def wontHang() {
        BookOperations.wontHang(bookService)
        redirect(controller: 'example')
    }

    def transaction() {
        BookOperations.transaction(jooqService)
        redirect(controller: 'example')
    }

    def reuse() {
        BookOperations.reuse(jooqService)
        redirect(controller: 'example')
    }
    
}
