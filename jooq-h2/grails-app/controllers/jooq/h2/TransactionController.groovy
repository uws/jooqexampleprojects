package jooq.h2

class TransactionController {
    def jooqService

    static defaultAction = "ok"

    def ok() {
        BookOperations.ok(jooqService)
        redirect(controller: 'example')
    }

    def rollback() {
        BookOperations.rollback(jooqService)
        redirect(controller: 'example')
    }
}
