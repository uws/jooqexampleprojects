<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
	</head>
	<body>
        <h2>Books:</h2>
        <code><g:each in="${books}">[${it.author} / ${it.name}] </g:each></code>
	</body>
</html>
