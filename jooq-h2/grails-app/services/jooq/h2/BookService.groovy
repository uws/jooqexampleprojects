package jooq.h2

import grails.transaction.Transactional

@Transactional
class BookService {

    def jooqService

    /**
     * Calling in transactional service context will reuse the connections
     * @return
     */
    def wontHung() {
        BookOperations.willHang(jooqService)
    }
}
